module MatchesModule

  def find_match(student, company, student_skills, company_skills, existing_matches)
    return if company_skills.length == 0
    return if company.vacancies <= 0
    return if company.match_value.nil?
    sum = 0

    company_skills.each do |company_skill|
      s_skill = student_skills.find{|s| s.skill_id == company_skill.skill_id}
      s_skill_level = s_skill.nil? ? 0 : s_skill.level
      c_skill_level = company_skill.level
      if (s_skill_level - c_skill_level + 2) < 0
        previous = existing_matches.find{|m| m.company_id == company.id}
        unless previous.nil?
          Match.delete(previous.id)
          # return {deleted: true, company_id: company.id, student_id: student.id}
        end
        return
      end
      sum += s_skill_level * c_skill_level
    end
    avg = sum / company_skills.map{|s| s.level}.inject(0, :+).to_f
    variance = company.match_value - avg

    previous = existing_matches.find{|m| m.company_id == company.id}

    if previous.nil?
      Match.create(company_id: company.id, student_id: student.id,
                   variance: variance)
    elsif previous.variance != variance
      Match.find(previous.id).update(variance: variance)
    end
    # return {company_id: company.id, student_id: student.id, variance: variance}
  end

end
