$(document).ready ->
  # Initializes jQuery datepicker
  $('#stage_start_time').datepicker
    changeMonth: true
    changeYear: true
    yearRange: '1900:<%= Date.today.year %>'
    minDate: '+1D'
    maxDate: '+100Y'
    dateFormat: 'dd/mm/yy'

  $('#stage_end_time').datepicker
    changeMonth: true
    changeYear: true
    yearRange: '1900:<%= Date.today.year %>'
    minDate: '+1D'
    maxDate: '+100Y'
    dateFormat: 'dd/mm/yy'

  return
