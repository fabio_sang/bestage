# Source: StackOverflow, http://stackoverflow.com/questions/45888/what-is-the-most-efficient-way-to-sort-an-html-selects-options-by-value-while

sortSelect = (select) ->
  my_options = $( select + ' option')
  my_options.sort (a, b) ->
    if a.text > b.text
      1
    else if a.text < b.text
      -1
    else
      0
  $(select).empty().append my_options
  return

# Exposes the method outside of the IIFE
window.sortSelect = sortSelect
