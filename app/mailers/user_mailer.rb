class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Bestage: Attivazione account"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Bestage: Reset della password"
  end
end
