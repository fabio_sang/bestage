module StudentsHelper
  # Returns the Gravatar for the given user.
  def gravatar_for(user, options = { size: 80, style: "gravatar" })
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    style = options[:style]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: style)
  end

  # Returns every skill that the given user doesn't have.
  def skills_not_owned(user)
    Skill.all - user.skills
  end

end
