module MatchesHelper

  def relative_match_percentage(match)
    (match.variance / 5 * 100).to_i
  end

  def absolute_match_percentage(match)
    (100 + relative_match_percentage(match))/2.to_f.round
  end

  def match_color(match)
    percentage = absolute_match_percentage(match)
    case percentage
      when 41..59         then "success"
      when 31..40, 60..69 then "yellow"
      when 21..30, 70..79 then "warning"
      when 0..20, 80..10  then "danger"
      else ""
    end
  end

  def match_arrow(match)
    percentage = absolute_match_percentage(match)

    arrow = ""
    color = ""
    case percentage
      when 51..100
        arrow = "\u25B2"
        color = "green"
      when 0..49
        arrow = "\u25BC"
        color = "red"
      else
        arrow = "="
        color = "black"
    end
    content_tag(:span, arrow, style: "color: #{color}; font-size: 10px")
  end

  def progress_bar(match)
    tag("div", class: ["progress-bar","progress-bar-#{match_color(match)}"],
               aria: {valuenow: 40, valuemin: 0, valuemax: 100},
               role: "progressbar",
               :style => "width: #{absolute_match_percentage(match)}%")
  end

end
