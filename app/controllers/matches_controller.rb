class MatchesController < ApplicationController

  before_action :logged_in_user, only: [:index, :show]

  def index
    stage = Stage.find_by(student_id: current_user.id)
    if !stage.nil?
      @stage = stage
      return
    end
    current_user.find_matches
    @matches = Match.where(student_id: current_user.id).
                     order("ABS(variance)").paginate(page: params[:page])
  end

  def show
    @match = Match.find(params[:id])
    @match_skills = []
    student = Student.find(@match.student_id)
    redirect_to root_url if student != current_user
    company = Company.find(@match.company_id)
    company_skills = company.company_skills
    student_skills = student.student_skills

    company_skills.each do |company_skill|
      s_skill = student_skills.find{|s| s.skill_id == company_skill.skill_id}
      skill = Skill.find(company_skill.skill_id).name
      s_skill_level = s_skill.nil? ? 0 : s_skill.level
      c_skill_level = company_skill.level
      @match_skills.push({name: skill, s_level: s_skill_level, c_level: c_skill_level})
    end
  end

  private

  # Before filters

  # Confirms a logged-in user.
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Devi essere loggato per eseguire quest'azione."
      redirect_to login_url
    end
  end

end
