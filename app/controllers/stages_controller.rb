class StagesController < ApplicationController

  before_action :logged_in_user, only: [:new, :create, :index, :edit,
                                        :update, :destroy]

  def new
    @stage = Stage.new
    match = Match.find(params[:id])
    @stage.student_id = match.student_id
    @stage.company_id = match.company_id
    student = Student.find(@stage.student_id)
    redirect_to root_url if current_user != student
    @match = { student: student.name,
               company: Company.find(@stage.company_id).name}
  end

  def create
    # beofre filter sull'user
    @stage = Stage.new(stage_params)
    if @stage.save
      flash[:info] = "Richiesta di inizio stage inviata"
      redirect_to root_url
    else
      flash[:danger] = "Errore nella richiesta"
      redirect_to root_url
    end
  end

  def index
    redirect_to root_url unless current_user.is_a? Company
    @stages = Stage.where(company_id: current_user.id, activated: false)
    @accepted_stages = Stage.where(company_id: current_user.id, activated: true)
  end

  def show
    @stage = Stage.find(params[:id])
    @company = Company.find(@stage.company_id)
    redirect_to root_url unless current_user == Student.find(@stage.student_id)
  end

  def update
    @stage = Stage.find(params[:id])
    company = Company.find(@stage.company_id)
    if company.update(vacancies: company.vacancies - 1) &&
        @stage.update(activated: true)
      flash[:success] = "Stage accettato"
      redirect_to stages_path
    else
      if company.vacancies < 0
        flash[:warning] = "Non hai più posti disponibili, non puoi più accettare stage."
      end
      redirect_to stages_path
    end
  end

  def destroy
    stage = Stage.find(params[:id])
    stage.destroy
    if current_user.is_a? Student
      flash[:success] = "Richiesta annullata"
      redirect_to matches_path
    else
      flash[:success] = "Studente rifiutato"
      redirect_to stages_path
    end
  end

  private

    def stage_params
      params.require(:stage).permit(:student_id, :company_id, :start_time,
                                    :end_time, :activated)
    end

    # Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Devi essere loggato per eseguire quest'azione."
        redirect_to login_url
      end
    end

end
