class StudentsController < ApplicationController
  include ApplicationHelper
  before_action :logged_in_user, only: [:new, :create, :index, :edit,
                                        :update, :destroy]
  before_action :correct_user,   only: [:show, :edit, :update]
  before_action :admin_user,     only: [:new, :create, :index, :destroy]

  def index
    @students = Student.order(:school_year).paginate(page: params[:page])
  end

  def show
    @student        = Student.find(params[:id])
    @student_skills = @student.student_skills
  end

  def new
    @student = Student.new
  end

  def create
    @student = Student.new(student_params)
    if does_alredy_exist? @student.email
      flash.now[:danger] = "L'email è già stata presa."
      render "new"
    elsif @student.save
      @student.send_activation_email
      flash[:info] = "E' stata inviata un'email per l'attivazione dell'account."
      redirect_to root_url
    else
      render "new"
    end
  end

  def edit
    @student = Student.find(params[:id])
    @student_skills = @student.skills
  end

  def update
    if @student.update_attributes(student_params)
      flash[:success] = "Profilo aggiornato"
      redirect_to @student
    else
      render "edit"
    end
  end

  def destroy
    Student.find(params[:id]).destroy
    flash[:success] = "Studente eliminato"
    redirect_to students_url
  end

  def update_skills
    @skills = Skill.all
    respond_to do |format|
      format.js
    end
  end

  def load_suggestions
    render json: Student.terms_for(params[:term])
  end

  def redirect
    s = Student.find_by(name: params[:name]).id
    redirect_to student_path(s) unless s.nil?
  end

  private

    def student_params
      params.require(:student).permit(:name, :email, :birth_date, :school_year,
                          :password, :password_confirmation,
                          student_skills_attributes: [:id, :skill_id, :level,
                                                      :_destroy] )
    end

    # Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Devi essere loggato per eseguire quest'azione."
        redirect_to login_url
      end
    end

    # Confirms the correct user (the student or the school).
    def correct_user
      @student = Student.find(params[:id])
      redirect_to(root_url) unless @student == current_user ||
                                   (current_user.is_a? School) ||
                                   (current_user.is_a? Company)
    end

    # Confirms an admin user (the school)
    def admin_user
      unless (current_user.is_a? School) && current_user.admin?
        flash[:danger] = "Devi essere loggato come admin
                          per eseguire quest'azione."
        redirect_to root_url
      end
    end

end
