class SchoolsController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update]
  before_action :correct_school, only: [:edit, :update]

  def index
    @school = School.first
  end

  def show
    @school = School.first
  end

  def edit
    @school = School.first
  end

  def update
    @school = School.find(params[:id])
    if @school.update_attributes(school_params)
      flash[:success] = "Profilo aggiornato"
      redirect_to @school
    else
      render "edit"
    end
  end

  private

    def school_params
      params.require(:school).permit(:name, :email, :description, :address,
                                     :password, :password_confirmation )
    end

    # Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Devi essere loggato come scuola
                          per eseguire questa azione."
        redirect_to login_url
      end
    end

    # Confirms the correct user.
    def correct_school
      @school = School.find(params[:id])
      redirect_to(root_url) unless current_user?(@school)
    end
end
