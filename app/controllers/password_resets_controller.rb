class PasswordResetsController < ApplicationController
  include ApplicationHelper
  before_action :get_user,         only: [:edit, :update]
  before_action :valid_user,       only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]

  def new
  end

  def create
    @user = find_user_by_email(params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      flash[:info] = "Un'email è stata inviata al tuo indirizzo di posta
                     elettronica con le istruzioni per il reset della password."
      redirect_to root_url
    else
      flash.now[:danger] = "Indirizzo email non trovato."
      render "new"
    end
  end

  def edit
  end

  def update
    if password_blank?
      flash.now[:danger] = "La password non può essere vuota"
      render "edit"
    elsif @user.update_attributes(user_params)
      log_in @user
      flash[:success] = "La password è stata resettata"
      redirect_to @user
    else
      render "edit"
    end
  end

  private

    def user_params
      params.require(user_to_sym(@user))
            .permit(:password, :password_confirmation)
    end

    # Returns ture if password is blank.
    def password_blank?
      params[user_to_sym(@user)][:password].blank?
    end

    # Before filters

    def get_user
      @user = find_user_by_email(params[:email])
    end

    # Confirms a valid user.
    def valid_user
      unless (@user && @user.activated? &&
              @user.authenticated?(:reset, params[:id]))
        redirect_to root_url
      end
    end

    # Checks expiration of reset token.
    def check_expiration
      if @user.password_reset_expired?
        flash[:danger] = "Il reset della password è scaduto."
        redirect_to new_password_reset_url
      end
    end

end
