class SkillsController < ApplicationController
  before_action :logged_in_user, only: [:index, :new, :create, :edit, :update,
                                        :destroy]
  before_action :admin_user, only: [:new, :create, :edit, :update, :destroy]

  def index
    @skills = Skill.paginate(page: params[:page])
  end

  def new
     @skill = Skill.new
  end

  def create
    @skill = Skill.new(skill_params)
    if @skill.save
      flash.now[:success] = "Nuova competenza creata."
      redirect_to new_skill_path
    else
      render "new"
    end
  end

  def edit
    @skill = Skill.find(params[:id])
  end

  def update
    @skill = Skill.find(params[:id])
    if @skill.update_attributes(skill_params)
      flash[:success] = "Competenza aggiornata"
      redirect_to skills_path
    else
      render "edit"
    end
  end

  def destroy
    Skill.find(params[:id]).destroy
    flash[:success] = "Competenza eliminata"
    redirect_to skills_url
  end

  private

    def skill_params
      params.require(:skill).permit(:name, :school_year)
    end

    # Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Devi essere loggato per eseguire quest'azione."
        redirect_to login_url
      end
    end

    # Confirms an admin user (the school)
    def admin_user
      unless (current_user.is_a? School) && current_user.admin?
        flash[:danger] = "Devi essere loggato come admin
                          per eseguire quest'azione."
        redirect_to root_url
      end
    end

end
