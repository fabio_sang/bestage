class Match < ActiveRecord::Base
  belongs_to :student
  belongs_to :company
  validates_presence_of :student
  validates_presence_of :company
  validates :student_id, uniqueness: { scope: :company_id }

  validates             :variance, presence: true, :inclusion => { :in => -5..5 }
end
