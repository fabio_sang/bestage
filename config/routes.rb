Rails.application.routes.draw do
  root 'static_pages#home'
  get    'stats'            => 'static_pages#stats'
  get    'about'           => 'static_pages#about'
  get    'contact'         => 'static_pages#contact'
  get    'login'           => 'sessions#new'
  post   'login'           => 'sessions#create'
  delete 'logout'          => 'sessions#destroy'
  resources :schools, :except => [:index]
  get       'schools'          => 'schools#show', :defaults => { :id => "1" }
  resources :students
  get       'students_signup'  => 'students#new'
  get       'students_load_suggestions' => 'students#load_suggestions'
  get       'students_redirect' => 'students#redirect'
  resources :companies
  get       'companies_signup' => 'companies#new'
  resources :skills
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :matches
  resources :stages

end
