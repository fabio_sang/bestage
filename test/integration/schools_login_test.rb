require 'test_helper'

class SchoolsLoginTest < ActionDispatch::IntegrationTest

  def setup
    @school = schools(:marconi)
  end

  test "login with invalid information" do
    get login_path
    assert_template "sessions/new"
    post login_path, session: { email: "", password: "" }
    assert_template "sessions/new"
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test "login with valid information followed by logout" do
    get login_path
    post login_path, session: { email: @school.email, password: "password" }
    assert is_logged_in?
    assert_redirected_to school_path(@school)
    follow_redirect!
    assert_template "schools/show"
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", school_path(@school)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    # Simulate a user clicking logout in a second window
    delete logout_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count: 0
    assert_select "a[href=?]", school_path(@school), count: 0
  end

  test "login with remembering" do
    log_in_as(@school, remember_me: '1')
    assert_not_nil cookies["remember_token"]
  end

  test "login without remembering" do
    log_in_as(@school, remember_me: '0')
    assert_nil cookies["remember_token"]
  end

end
