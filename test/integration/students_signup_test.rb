require 'test_helper'

class StudentsSignupTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
    @admin = School.first
    log_in_as(@admin)
  end

  test "invalid signup information" do
    get students_signup_path
    assert_no_difference "Student.count" do
      post students_path, student: { name:        "",
                                     email:       "student@invalid",
                                     birth_date:  Date.tomorrow,
                                     school_year: 33,
                                     password:              "foo",
                                     password_confirmation: "bar" }
    end
    assert_template "students/new"
  end

  test "email alredy taken by other model" do
    get students_signup_path
    assert_no_difference "Student.count" do
      post_via_redirect students_path,
                        student: {name:        "Example Student",
                                  email:       schools(:marconi).email,
                                  birth_date:  Date.yesterday,
                                  school_year: 3,
                                  password:              "foobar",
                                  password_confirmation: "foobar" }
    end
    assert_not flash.empty?
    assert_template "students/new"
  end

  test "valid signup information with account activation" do
    get students_signup_path
    assert_difference "Student.count", 1 do
      post students_path, student: {name:        "Example Student",
                                    email:       "student@valid.com",
                                    birth_date:  Date.yesterday,
                                    school_year: 3,
                                    password:              "foobar",
                                    password_confirmation: "foobar" }
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    student = assigns(:student)
    assert_not student.activated?
    log_out
    # Try to log in before activation.
    log_in_as(student)
    assert_not is_logged_in?
    # Invalid activation token
    get edit_account_activation_path("invalid token")
    assert_not is_logged_in?
    # Valid token, wrong email
    get edit_account_activation_path(student.activation_token, email: 'wrong')
    assert_not is_logged_in?
    # Valid activation token
    get edit_account_activation_path(student.activation_token,
                                     email: student.email)
    assert student.reload.activated?
    follow_redirect!
    assert_template 'students/show'
    assert is_logged_in?
  end

end
