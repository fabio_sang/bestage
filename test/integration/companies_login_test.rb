require 'test_helper'

class CompaniesLoginTest < ActionDispatch::IntegrationTest
  require 'test_helper'

  def setup
    @company = companies(:abb)
  end

  test "login with invalid information" do
    get login_path
    assert_template "sessions/new"
    post login_path, session: { email: "", password: "" }
    assert_template "sessions/new"
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test "login with valid information followed by logout" do
    get login_path
    post login_path, session: { email: @company.email, password: "password" }
    assert is_logged_in?
    assert_redirected_to company_path(@company)
    follow_redirect!
    assert_template "companies/show"
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", company_path(@company)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    # Simulate a user clicking logout in a second window
    delete logout_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count: 0
    assert_select "a[href=?]", company_path(@company), count: 0
  end

  test "login with remembering" do
    log_in_as(@company, remember_me: '1')
    assert_not_nil cookies["remember_token"]
  end

  test "login without remembering" do
    log_in_as(@company, remember_me: '0')
    assert_nil cookies["remember_token"]
  end

end
