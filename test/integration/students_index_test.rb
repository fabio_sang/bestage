require 'test_helper'

class StudentsIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin = schools(:marconi)
    @non_admin = students(:archer)
  end

  test "index as admin including pagination and delete links" do
    log_in_as(@admin)
    get students_path
    assert_template 'students/index'
    assert_select 'div.pagination'
    first_page_of_students = Student.paginate(page: 1)
    first_page_of_students.each do |student|
      assert_select 'a[href=?]', student_path(student), text: student.name
      unless student == @admin
        assert_select 'a[href=?]', student_path(student), text: 'elimina',
        method: :delete
      end
    end
    assert_difference 'Student.count', -1 do
      delete student_path(@non_admin)
    end
  end

  test "index as non-admin" do
    log_in_as(@non_admin)
    get students_path
    assert_select 'a', text: 'elimina', count: 0
  end

end
