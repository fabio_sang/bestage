require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  test "account_activation for schools" do
    user = schools(:marconi)
    user.activation_token = School.new_token
    mail = UserMailer.account_activation(user)
    assert_equal "Bestage: Attivazione account", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["me@db82fef59e129939a91c1c11fc5ca54b.com"], mail.from
    assert_match user.name, mail.body.encoded
    assert_match user.activation_token, mail.body.encoded
    assert_match CGI::escape(user.email), mail.body.encoded
  end

  test "password_reset for schools" do
    user = schools(:marconi)
    user.reset_token = School.new_token
    mail = UserMailer.password_reset(user)
    assert_equal "Bestage: Reset della password", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["me@db82fef59e129939a91c1c11fc5ca54b.com"], mail.from
    assert_match user.reset_token, mail.body.encoded
    #assert_match CGI::escape(user.email), mail.body.encoded
  end

  test "account_activation for students" do
    user = students(:archer)
    user.activation_token = Student.new_token
    mail = UserMailer.account_activation(user)
    assert_equal "Bestage: Attivazione account", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["me@db82fef59e129939a91c1c11fc5ca54b.com"], mail.from
    assert_match user.name, mail.body.encoded
    assert_match user.activation_token, mail.body.encoded
    assert_match CGI::escape(user.email), mail.body.encoded
  end

  test "password_reset for students" do
    user = students(:archer)
    user.reset_token = Student.new_token
    mail = UserMailer.password_reset(user)
    assert_equal "Bestage: Reset della password", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["me@db82fef59e129939a91c1c11fc5ca54b.com"], mail.from
    assert_match user.reset_token, mail.body.encoded
    #assert_match CGI::escape(user.email), mail.body.encoded
  end

  test "account_activation for companies" do
    user = companies(:abb)
    user.activation_token = Company.new_token
    mail = UserMailer.account_activation(user)
    assert_equal "Bestage: Attivazione account", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["me@db82fef59e129939a91c1c11fc5ca54b.com"], mail.from
    assert_match user.name, mail.body.encoded
    assert_match user.activation_token, mail.body.encoded
    assert_match CGI::escape(user.email), mail.body.encoded
  end

  test "password_reset for companies" do
    user = companies(:abb)
    user.reset_token = Company.new_token
    mail = UserMailer.password_reset(user)
    assert_equal "Bestage: Reset della password", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["me@db82fef59e129939a91c1c11fc5ca54b.com"], mail.from
    assert_match user.reset_token, mail.body.encoded
    #assert_match CGI::escape(user.email), mail.body.encoded
  end

end
