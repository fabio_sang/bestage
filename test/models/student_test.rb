require 'test_helper'

class StudentTest < ActiveSupport::TestCase

  def setup
    @student = Student.new(name:        "Example Student",
                           email:       "student@example.com",
                           password:    "foobar",
                           password_confirmation: "foobar",
                           birth_date:  17.years.ago,
                           school_year: 3)
  end

  test "should be valid" do
    assert @student.valid?
  end

  test "name should be present and not too long" do
    @student.name = "   "
    assert_not @student.valid?
    @student.name = "a" * 51
    assert_not @student.valid?
  end

  test "email should be present and not too long" do
    @student.email = "    "
    assert_not @student.valid?
    @student.email = "a" * 244 + "@example.com"
    assert_not @student.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
    first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @student.email = valid_address
      assert @student.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
    foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @student.email = invalid_address
      assert_not @student.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email addresses should be unique" do
    duplicate_student = @student.dup
    duplicate_student.email = @student.email.upcase
    @student.save
    assert_not duplicate_student.valid?
  end

  test "password should have a minimum length" do
    @student.password = @student.password_confirmation = "a" * 5
    assert_not @student.valid?
  end

  test "birth_date should be present and valid" do
    @student.birth_date = nil
    assert_not @student.valid?
    @student.birth_date = 3.years.from_now
    assert_not @student.valid?
    @student.birth_date = 15.years.ago
    assert @student.valid?
  end

  test "school year should be present and valid" do
    @student.school_year = nil
    assert_not @student.valid?
    @student.school_year = 6
    assert_not @student.valid?
    @student.school_year = 5
    assert @student.valid?
  end

  test "should assign and remove a skill" do
    skill = skills(:sample)
    student = students(:archer)
    student.assign_skill(skill, 1)
    assert student.has_skill?(skill)
    student.remove_skill(skill)
    assert_not student.has_skill?(skill)
  end

end
