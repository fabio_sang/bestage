require 'test_helper'

class SchoolTest < ActiveSupport::TestCase

  def setup
    @school = School.new( name: "Example School", email: "school@example.com",
                          description: "Example description of school",
                          address: "Example Street 18, Example Country",
                          password: "foobar", password_confirmation: "foobar" )
  end

  test "should be valid" do
    assert @school.valid?
  end

  test "name should be present" do
    @school.name = "   "
    assert_not @school.valid?
  end

  test "description should be present" do
    @school.description = "   "
    assert_not @school.valid?
  end

  test "address should be present" do
    @school.address = "   "
    assert_not @school.valid?
  end

  test "email should be present" do
    @school.email = "   "
    assert_not @school.valid?
  end

  test "name should not be too long" do
    @school.name = "a" * 51
    assert_not @school.valid?
  end

  test "email should not be too long" do
    @school.email = "a" * 244 + "@example.com"
    assert_not @school.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @school.email = valid_address
      assert @school.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @school.email = invalid_address
      assert_not @school.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @school.email = mixed_case_email
    @school.save
    assert_equal mixed_case_email.downcase, @school.reload.email
  end

  test "password should have a minimum length" do
    @school.password = @school.password_confirmation = "a" * 5
    assert_not @school.valid?
  end

  test "authenticated? should return false for a school with nil digest" do
    assert_not @school.authenticated?(:remember, "")
  end

end
