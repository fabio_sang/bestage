require 'test_helper'

class SkillTest < ActiveSupport::TestCase

  def setup
    @skill = Skill.new(name: "Example name", school_year: 2)
  end

  test "should be valid" do
    assert @skill.valid?
  end

  test "name should be present" do
    @skill.name = "   "
    assert_not @skill.valid?
  end

  test "school year should be present and in 1..5" do
    @skill.school_year = nil
    assert_not @skill.valid?
    @skill.school_year = 0
    assert_not @skill.valid?
  end

end
