require 'test_helper'

class CompanyTest < ActiveSupport::TestCase

  def setup
    @company = Company.new(name: "Example Company",
                           email: "company@example.com",
                           description: "foo",
                           address: "bar",
                           vacancies: 2,
                           password: "foobar",
                           password_confirmation: "foobar" )
  end

  test "should be valid" do
    assert @company.valid?
  end

  test "name should be present" do
    @company.name = "    "
    assert_not @company.valid?
  end

  test "email should be present" do
    @company.email = "    "
    assert_not @company.valid?
  end

  test "name should not be too long" do
    @company.name = "a" * 51
    assert_not @company.valid?
  end

  test "email should not be too long" do
    @company.email = "a" * 244 + "@example.com"
    assert_not @company.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
    first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @company.email = valid_address
      assert @company.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
    foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @company.email = invalid_address
      assert_not @company.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email addresses should be unique" do
    duplicate_user = @company.dup
    duplicate_user.email = @company.email.upcase
    @company.save
    assert_not duplicate_user.valid?
  end

  test "password should have a minimum length" do
    @company.password = @company.password_confirmation = "a" * 5
    assert_not @company.valid?
  end

  test "vacancies should be greater than or equal to 0" do
    @company.vacancies = 0
    assert @company.valid?
  end

  test "authenticated? should return false for a user with nil digest" do
    assert_not @company.authenticated?(:remember, '')
  end

  test "should assign and remove a skill" do
    skill = skills(:sample)
    company = companies(:abb)
    company.assign_skill(skill, 1)
    assert company.has_skill?(skill)
    company.remove_skill(skill)
    assert_not company.has_skill?(skill)
  end

end
