School.create!(name:         "Scuola Esempio",
               email:        "scuola@esempio.com",
               description:  "Descrizione esempio per scuola esempio",
               address:      "Indirizzo esempio",
               password:              "foobar",
               password_confirmation: "foobar",
               activated:    true,
               activated_at: Time.zone.now,
               admin:        true )

Student.create!(name:        "Studente Esempio",
                email:       "studente@esempio.com",
                birth_date:  15.years.ago,
                school_year: "3a",
                password:              "foobar",
                password_confirmation: "foobar",
                activated:    true,
                activated_at: Time.zone.now )

Company.create!(name:        "Azienda Esempio",
                email:       "azienda@esempio.com",
                description: "Descrizione esempio",
                address:     "Indirizzo esempio",
                vacancies:   2,
                password:             "foobar",
                password_confirmation:"foobar",
                activated:    true,
                activated_at: Time.zone.now )

puts "Started creating students...".brown
634.times do |n|
  name =        Faker::Name.name
  email =       "example-#{n+1}@bestage.com"
  birth_date =  Faker::Time.between(14.years.ago, 20.years.ago)
  school_year = "#{rand(1..5)}#{('a'..'z').to_a.sample}"
  password =    "password"

  Student.create!(name: name,
                  email: email,
                  birth_date: birth_date,
                  school_year: school_year,
                  password: password,
                  password_confirmation: password,
                  activated:    true,
                  activated_at: Time.zone.now )
  print "."
end
puts ""
puts "Students created!".green

puts "Started creating companies...".brown
241.times do |n|
  name =        Faker::Company.name
  email =       "company-#{n+1}@bestage.com"
  description = Faker::Lorem.sentence
  address =     Faker::Address.street_address
  vacancies =   rand(1..5)
  password =    "password"

  Company.create!( name: name,
                   email: email,
                   description: description,
                   address: address,
                   vacancies: vacancies,
                   password: password,
                   password_confirmation: password,
                   activated:    true,
                   activated_at: Time.zone.now )

  print "."
end
puts ""
puts "Companies created!".green

puts "Started creating skills...".brown
53.times do
  skill =       Faker::Lorem.paragraph(3)
  school_year = rand(1..5)
  course =      ["Informatica", "Elettronica", "Trasversale", "Meccanica",
                 "Meccatronica", "Energia", "Elettrotecnica"].sample #Faker::Educator.course

  Skill.create!( name: skill,
                 school_year: school_year,
                 course: course)
  print "."
end
puts ""
puts "Skills created!".green

puts "Started creating student-skill relationships...".brown
students =  Student.all
companies = Company.all
skills =    Skill.first(20)

students.each do |student|
  random = rand(5..10)
  rand_skills = [[1,2,3],[2,3,4],[3,4,5],[1,2,3,4,5]].sample
  random.times {StudentSkill.create(student_id: student.id,
                                     skill_id:   skills.sample.id,
                                     level:      rand_skills.sample)}
  print "."
end
puts ""
puts "Student-skill relationships created!".green

puts "Started creating company-skill relationships...".brown
companies.each do |company|
  random = rand(5..10)
  rand_skills = [[1,2,3],[2,3,4],[3,4,5],[1,2,3,4,5]].sample
  random.times {CompanySkill.create(company_id: company.id,
                                     skill_id:   skills.sample.id,
                                     level:      rand_skills.sample)}
  company.update(match_value: company.calculate_match_value)
  print "."
end
puts ""
puts "Company-skill relationships created!".green

puts "Started creating stage relationships...".brown
students = Student.last(152)
companies = Company.last(43)

students.each do |s|
  company = companies[rand(0..42)]
  Stage.create!(student_id: s.id,
                company_id: company.id,
                start_time: 5.days.from_now,
                end_time:   6.days.from_now,
                activated:  true)
  print "."
end
Student.first(4).each{|s| Stage.create!(company_id: 1, student_id: s.id,
                                        start_time: 2.days.from_now,
                                        end_time: 4.days.from_now,
                                        activated: false)}

puts ""
puts "Stage relationships created!".green
