class ChangeTypeToSchoolYearOfStudents < ActiveRecord::Migration
  def change
    change_column :students, :school_year, :string
  end
end
