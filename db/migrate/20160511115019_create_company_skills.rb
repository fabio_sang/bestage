class CreateCompanySkills < ActiveRecord::Migration
  def change
    create_table :company_skills do |t|
      t.integer :skill_id
      t.integer :company_id
      t.integer :level

      t.timestamps null: false
    end
  end
end
