class CreateStudentSkills < ActiveRecord::Migration
  def change
    create_table :student_skills do |t|
      t.integer :skill_id
      t.integer :student_id
      t.integer :level

      t.timestamps null: false
    end
  end
end
