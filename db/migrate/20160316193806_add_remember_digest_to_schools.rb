class AddRememberDigestToSchools < ActiveRecord::Migration
  def change
    add_column :schools, :remember_digest, :string
  end
end
