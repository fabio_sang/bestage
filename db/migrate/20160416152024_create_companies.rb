class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :email
      t.string :description
      t.string :address
      t.integer :vacancies

      t.timestamps null: false
    end
  end
end
