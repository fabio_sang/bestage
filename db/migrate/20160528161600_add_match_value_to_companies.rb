class AddMatchValueToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :match_value, :float
  end
end
