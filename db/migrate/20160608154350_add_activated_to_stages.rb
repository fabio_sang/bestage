class AddActivatedToStages < ActiveRecord::Migration
  def change
    add_column :stages, :activated, :boolean, default: false
  end
end
